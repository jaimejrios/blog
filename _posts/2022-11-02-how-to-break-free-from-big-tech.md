---
layout: post
title: How To Break Free <br> From Big Tech
summary: How would I replace essential tools like Gmail, Apple Notes, and the Google Drive? I used Google and Apple software religiously. Big tech had me in metaphorical chains. And I was determined to break free.
featured-img: break-free-big-tech
categories: Privacy
---

# Adios Big Tech, Hello Privacy

I remember when I started purging big tech from my digital life ([this blog post](https://blog.jaimejrios.com/the-1-reason-to-ditch-google/) explains what compelled me to do this).

The problem was I didn't know exactly how to go about it.

How would I replace essential tools like Gmail, Apple Notes, and the Google Drive?

I used Google and Apple software religiously.

Big tech had me in metaphorical chains. And I was determined to break free.

It really came down to finding suitable replacements for things like Google Drive and Apple Notes.

But how would I find privacy-respecting alternatives to the mainstream tools I used daily.

What helped was discovering [alternativeto.net](https://alternativeto.net/).

This site provides a way to search for alternative software for specific applications.

After trialing a plethora of alternative applications, I was able to find quite a few that suited my use cases.

Even better, these alternative apps were privacy-centric and not part of Apple's and Google's ecosystem.

I could finally purge big tech from my life (thank goodness!)

In this post, I want to share some of the applications that helped me break free from big tech's clutches.

Here's a rundown of those apps...

---

## The best alternatives to Gmail.

**#1 - [Proton Mail](https://proton.me/mail/)**

You've probably heard about this application already. And for good reason.

I genuinely believe Proton Mail (and all Proton-based services) will be the Google killer in the foreseeable future.

Proton is a Switz-based company that provides Google-like services that are privacy-centric.

They also provide end-to-end encryption features for keeping your data secure and private (unlike Google).

Out of all the privacy-focused email alternatives to Gmail, Proton would be my #1 email service to recommend.

While this post isn't necessarily a review article, but more of a personal recommendation list, I will say that I've yet to encounter any deal-breaking software bugs while using Proton Mail.

This app works as it should as far as sending and receiving email.

It's worth nothing they offer 1GB of email storage for free-tier plans.

Their mobile and desktop apps are also updated regularly to ensure consistent quality across devices.

**#2 - [Tutanota](https://tutanota.com)**

You probably haven't heard about this application.

While I don't believe Tutanota will be the Google killer, I think they offer an excellent alternative to Gmail.

Tutanota is a German-based company that provides privacy-focused email services with end-to-end encryption features for keeping your data secure and private (again, unlike Google).

Like Proton Mail, they offer 1GB of email storage for free-tier plans. Mobile apps for Android and iOS are also available.

The reason this app is number two on this particular list is because I encountered a massive bug while using Tutanota.

There was a time period of 24-48 hours where I could not log into my account to check for new emails (the login screen kept trying to refresh after entering my username and password).

This happened about a year ago. Luckily it wasn't a deal-breaker that kept me from using the app.

After the app was updated in timely fashion, it ran smoothly and worked.

Despite this incident, I still highly recommend Tutanota.

The app maintainers continue to treat their users with care, despite when things go awry at times.

---

## The best note-taking app substitute.

**#1 - [Standard Notes](https://standardnotes.com)**

Both Google's and Apple's note-taking app have been a staple in the laptops and smartphones of many.

This isn't the case for me anymore.

Standard Notes has effectively replaced Apple Notes as my go-to, note-taking application.

Standard Notes provides basic functionality like Google and Apple Notes, but with privacy and security in mind.

Like the other apps mentioned in this post, Standard Notes provides privacy-focused, note-taking services coupled with end-to-end encryption features for keeping your notes secure and private.

Although Standard Notes isn't entirely perfect, it is updated regularly to make sure core functionality operates smoothly.

The one time I did encounter a bug was a few months ago. My notes wouldn't sync up across devices.

Although I had to downgrade to the previous version until the bug was fixed a few months later, it was well worth the patience to stick with Standard Notes.

After the required updates were implemented, I've yet to encounter issues on both desktop and mobile devices.

---

## The best alternatives to iCloud and Google Drive.

**#1 - [Nextcloud](https://nextcloud.com/sign-up/)**

I'm not surprised that Google Drive and iCloud are favorites for cloud storage providers.

Being easy to setup and incredibly accessible, I actually found myself having a tough time finding a replacement.

That was until I came across Nextcloud.

Nextcloud is a German-based company that provides cloud-based software for decentralized and federated clouds.

The reason Nextcloud is number one on this list is because of their free-tier service (in addition to being privacy-centric and [open source]([open source](https://en.wikipedia.org/wiki/Open-source_software)) of course).

At the time of this writing, they offer free-tier plans with up to 8GB of storage! (which is unheard of for free, open source software)

While I've encountered issues where my files wouldn't sync with the cloud after modifying said files, this problem rarely surfaced.

Nextcloud eventually phased out the bug in a later update.

Having used this app for nearly a year now, I can attest that core functionality operates reliably.

**#2 - [Proton Drive](https://proton.me/drive/pricing)**

As previously mentioned, I do consider Proton as a genuine threat to Google.

And unlike Google, all Proton apps are [open source](https://en.wikipedia.org/wiki/Open-source_software) and have been independently audited and verified by [third-party experts](https://www.cnet.com/news/privacy/protonvpn-clears-its-latest-no-logs-audit/).

Proton has already created a rock-solid, privacy-centric email application.

And along with their other apps, which includes [Calendar]([open source](https://en.wikipedia.org/wiki/Open-source_software)), [VPN](https://protonvpn.com/), and [Cloud Storage](https://proton.me/drive), it speaks volumes about Proton's positive intentions within the privacy space.

While Proton Drive's 1GB free-tier plan isn't as gracious as Nextcloud's 5GB and 8GB free-tier plans, I think it's worth mentioning that their [Drive-Plus](https://proton.me/drive/pricing?product=drive) paid plan is only $3.99 per month for 200GB of storage.

---

## Share your thoughts...

Feel free to leave a comment about your experiences using open source, privacy-centric applications.

Were they positive or negative?

Anything else you'd like to add? Share below...
