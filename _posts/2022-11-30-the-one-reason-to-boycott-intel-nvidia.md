---
layout: post
title: Is Now The Time To Boycott Intel and Nvidia?
summary: China uses Intel and Nvidia computer chips and graphics cards to power their start-of-the-art, surveillance technology. This tech is used primarily for suppression and fighting against so-called "terrorism" within China.
featured-img: intel-nvidia-red-x-hero
categories: Opinions
---

# Intel And Nvidia Have Some Explaining To Do

I was shocked when I learned about Intel's and Nvidia's involvement with China's surveillance state.

In case you weren't aware, China uses Intel and Nvidia computer chips and graphics cards to power their start-of-the-art, surveillance technology.

This tech is used primarily for suppression and combating the so-called "terrorism" problem within China.

This has been confirmed from multiple sources such as the [Washington Post](https://www.washingtonpost.com/opinions/us-made-technologies-are-aiding-chinas-surveillance-of-uighurs-how-should-washington-respond/2020/11/26/0218bbb4-2dc9-11eb-bae0-50bb17126614_story.html), [Al Jazeera](https://www.aljazeera.com/economy/2020/12/9/intel-nvidia-probed-over-tech-allegedly-used-against-uighurs), [Reuters](https://www.reuters.com/world/china/us-lawmakers-ask-intel-nvidia-about-sale-tech-china-used-against-uighurs-2020-12-11/), as well as the [NY Times](https://www.nytimes.com/2020/11/22/technology/china-intel-nvidia-xinjiang.html).

This is why I'm planning to jump ship from Intel and Nvidia altogether.

While I won't go into full detail regarding this matter (since the above article links already do a good job of that), I will explain why this issue is especially concerning for me, being a heavy computer user and consumer.

I think it's both disappointing and frustrating that the major companies who produce our computer chips and graphics cards have ties to mass surveillance and suppression.

Simply put, I feel guilty knowing that the computer products I use are made by companies who do business with countries that are known to silence minorities and those opposed to an anti-democratic state.

Take for example the Uighur situation in China's Xinjiang region.

[Reuters reported](https://www.reuters.com/world/china/us-lawmakers-ask-intel-nvidia-about-sale-tech-china-used-against-uighurs-2020-12-11/) that 'according to the United Nations, they've estimated more than a million Chinese Muslims have been detained in forced "re-education" camps in the Xinjiang region.'

It is also majorly disconcerting that "Intel and Nvidia chips power a supercomputing center that tracks people in a place where government suppresses minorities, raising questions about the tech industry’s responsibility" [according to the NY Times](https://www.nytimes.com/2020/11/22/technology/china-intel-nvidia-xinjiang.html).

The fact that the Chinese government uses Intel and Nvidia technology to power their high-powered surveillance system to monitor and essentially subdue the Uighur population is incredibly upsetting.

[The Al Jazeera news](https://www.aljazeera.com/economy/2020/12/9/intel-nvidia-probed-over-tech-allegedly-used-against-uighurs) recorded that 'The US State Department has accused Chinese officials of subjecting Uighur Muslims to torture, abuse "and trying to basically erase their culture and their religion".'

While it is possible that Intel and Nvidia were unaware their products were being used for nefarious purposes, it's their response to these allegations that really irked me.

[According to a CNN report](https://www.cnn.com/2021/12/23/business/intel-china-apology-xinjiang-intl-hnk/index.html), 'The US chipmaker Intel, told suppliers in a letter dated December 2021 that it "is required to ensure our supply chain does not use any labor or source goods or services from the Xinjiang region" of China, citing government restrictions and questions from investors and customers.'

But Intel backtracked on this statement because of backlash from their Chinese partners.

[Intel stated](https://www.cnn.com/2021/12/23/business/intel-china-apology-xinjiang-intl-hnk/index.html) on Weibo, "Although our original intention was to ensure compliance with US laws, this letter has caused many questions and concerns among our cherished Chinese partners, which we deeply regret."

I think if Intel cared more about human rights rather than profits gained from their Chinese partners, there wouldn't be anything to regret about. Let alone making this sort of statement.

While Intel was left backtracking, Nvidia declined to comment.

Really Nvidia?

You have absolutely nothing to say about the fact that your chips and graphics cards are used to help surveil and censor Xinjiang’s Muslim-based community.

On the subject of surveillance, it's mind-boggling to think that China's supercomputers can 'analyze 1,000 video feeds simultaneously and search more than 100 million photos in a single second. They monitor cars, phones and faces — putting together patterns of behavior for "predictive policing" that justifies snatching people off the street for imprisonment or so-called reeducation.' (source: [Washington Post](https://www.washingtonpost.com/opinions/us-made-technologies-are-aiding-chinas-surveillance-of-uighurs-how-should-washington-respond/2020/11/26/0218bbb4-2dc9-11eb-bae0-50bb17126614_story.html))

It goes without saying that the next computer I purchase will not carry an Intel or Nvidia product.

For me personally, this does not align with my ethical and moral values.

Although my current workstations carry Intel and Nvidia hardware, I will certainly not be purchasing product from those two companies anytime soon.

As far as my next computer goes, I'll be buying an [AMD](https://www.amd.com/en) or [ARM-based](https://www.arm.com/) system.

That's for damn sure.

While it's ultimately your sole discretion on whether or not to buy from Intel and Nvidia, hopefully this blog post enlightens you on this highly controversial topic.

And hopefully I've convinced you in some capacity to consider boycotting Intel and Nvidia altogether.
