---
layout: post
title: The Easiest Way To Wipe A Hard Drive In Linux
summary: Wipe is a nifty program that helps make securely erasing data on a storage device quick and painless. All you need is a Debian-based, Linux operating system.
featured-img: linux-penguin
categories: Guides
---

# Wiping Your Hard Drive Can't Get Much Easier

So maybe you've sold your hard drive.

Or perhaps you're giving it away to someone.

And now you need to find a way to securely erase all data on it.

Forever...

The good news is that Linux has a tool for it.

While it's only available on Debian-based distros (i.e. Ubuntu, Linux Mint, etc.), this nifty tool makes wiping data on a storage device a breeze.

Now let's get started with the steps on how to do this.

## Step 1 - Locating The Storage Device

Before we start erasing data from our hard drive, it's important we make sure we're selecting the correct storage device.

The last thing you want is to wipe the storage device that contains your operating system.

So open up your Terminal application, and enter the following command:

`$ lsblk`

The `lsblk` command lists all of the storage devices connected to your computer.

In our case, we will be using the `lsblk` command to identify the correct device name so we can wipe the data on it securely.

After entering the `lsblk` command, you'll be able to view storage device information like so:

```
$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0  29.8G  0 disk
├─sda1   8:1    0   286M  0 part /boot/efi
├─sda2   8:2    0     2G  0 part [SWAP]
└─sda3   8:3    0  27.6G  0 part /
sdb      8:16   0 238.5G  0 disk
├─sdb1   8:17   0   100M  0 part
├─sdb2   8:18   0    16M  0 part
├─sdb3   8:19   0 237.8G  0 part
└─sdb4   8:20   0 597.2M  0 part
```

Here we do not want to delete the `sda` storage device. Since it contains our operating system.

We know this since the `/boot/efi` and `[SWAP]` partitions are listed as `sda1` and `sda2` respectively (both `sda1` and `sda2` are part of the `sda` storage device).

While I won't go into detail on what `/boot/efi` and `[SWAP]` partitions are used for, just know they're essential components of an operating system.

Here in our example, the storage device we're looking for is `sdb`. This is the storage device we'll be erasing all data from.

The location path for the `sdb` storage device is `/dev/sdb`.

If you connected two external storage devices to your computer for example, the location path for those devices would be `/dev/sdb` and `/dev/sdc` respectively.

## Step 2: Install The Wipe program

Now we know the location path of the storage device, it's time to install Wipe.

Enter in the following command in the Terminal to install it:

```
sudo apt install wipe -y
```

Again, this command only works for Debian-based distros.

## Step 3: Wiping Your Storage Device

After installing Wipe, we can now securely erase all data on the storage device.

For this example, we'll be wiping data from the `/dev/sdb` storage device.

Remember that this path could be different in your case. You could be wiping data from `/dev/sdc` or `/dev/sdd`.

Once you have the location path of your storage device, you can use the Wipe program like so:

```
wipe /dev/sdb
```

Depending on how large your storage device is and the processing speed of your computer, it could take some time.

Just wait patiently until the wipe process completes.

And that is it!

You've just learned how to securely erase data from a storage device using Wipe.

---

## Share your thoughts...

Feel free to leave a comment about your experiences erasing data from storage devices on Linux.

Were they positive or negative?

Anything else you'd like to add? Share below...
