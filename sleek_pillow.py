import PIL
from PIL import Image
import os, sys

src_path = os.getcwd() + '/_img/posts'
export_path = os.getcwd() + '/assets/img/posts/'
img_formats = ('jpg', 'png')

def generate_images(directory=False, is_optimized=True, img_quality=50):
    if directory:
        os.chdir(directory)

    files = os.listdir()
    images = [file for file in files if file.endswith(img_formats)]

    for image in images:
        print('Generating images for ' + image)

        image_name = image.rsplit('.', 1)[0]
        image_ext = image.rsplit('.', 1)[1]

        img = Image.open(image)
        img_width = img.size[0]
        img_height = img.size[1]

        placehold_width_fixed = 230
        placehold_width_percent = placehold_width_fixed / img_width
        placehold_height = int(img_height * float(placehold_width_percent))
        placehold_ext = image_name + '_placehold.' + image_ext

        thumb_width_fixed = 535
        thumb_width_percent = thumb_width_fixed / img_width
        thumb_height = int(img_height * float(thumb_width_percent))
        thumb_ext = image_name + '_thumb.' + image_ext

        thumb2x_width_fixed = 535 * 2
        thumb2x_width_percent = thumb2x_width_fixed / img_width
        thumb2x_height = int(img_height * float(thumb2x_width_percent))
        thumb2x_ext = image_name + '_thumb@2x.' + image_ext

        xs_width_fixed = 575
        xs_width_percent = xs_width_fixed / img_width
        xs_height = int(img_height * float(xs_width_percent))
        xs_ext = image_name + '_xs.' + image_ext

        sm_width_fixed = 767
        sm_width_percent = sm_width_fixed / img_width
        sm_height = int(img_height * float(sm_width_percent))
        sm_ext = image_name + '_sm.' + image_ext

        md_width_fixed = 991
        md_width_percent = md_width_fixed / img_width
        md_height = int(img_height * float(md_width_percent))
        md_ext = image_name + '_md.' + image_ext

        lg_width_fixed = 1999
        lg_width_percent = lg_width_fixed / img_width
        lg_height = int(img_height * float(lg_width_percent))
        lg_ext = image_name + '_lg.' + image_ext

        main_width_fixed = 1920
        main_width_percent = main_width_fixed / img_width
        main_height = int(img_height * float(main_width_percent))
        main_ext = image_name + '.' + image_ext

        img_placehold = img.resize((placehold_width_fixed, placehold_height), Image.ANTIALIAS)
        img_placehold.save(export_path + placehold_ext, optimize=is_optimized, quality=img_quality)

        img_thumb = img.resize((thumb_width_fixed, thumb_height), Image.ANTIALIAS)
        img_thumb.save(export_path + thumb_ext, optimize=is_optimized, quality=img_quality)

        img_thumb2x = img.resize((thumb2x_width_fixed, thumb2x_height), Image.ANTIALIAS)
        img_thumb2x.save(export_path + thumb2x_ext, optimize=is_optimized, quality=img_quality)

        img_xs = img.resize((xs_width_fixed, xs_height), Image.ANTIALIAS)
        img_xs.save(export_path + xs_ext, optimize=is_optimized, quality=img_quality)

        img_sm = img.resize((sm_width_fixed, sm_height), Image.ANTIALIAS)
        img_sm.save(export_path + sm_ext, optimize=is_optimized, quality=img_quality)

        img_md = img.resize((md_width_fixed, md_height), Image.ANTIALIAS)
        img_md.save(export_path + md_ext, optimize=is_optimized, quality=img_quality)

        img_lg = img.resize((lg_width_fixed, lg_height), Image.ANTIALIAS)
        img_lg.save(export_path + lg_ext, optimize=is_optimized, quality=img_quality)

        img_main = img.resize((main_width_fixed, main_height), Image.ANTIALIAS)
        img_main.save(export_path + main_ext, optimize=is_optimized, quality=img_quality)

    print('')

def clean_images(path):
    print('\nCleaning images in ' + path + '\n')
    files = os.listdir(path)
    for file in files:
        os.remove(path + file)

def main():

    if (len(sys.argv) > 1):
        if (sys.argv[1].lower() == "-c" or sys.argv[1].lower() == "--clean-only"):
            clean_images(export_path)
    else:
        #clean_images(export_path)
        generate_images(src_path)

main()
