---
layout: post
title: The  &#35;1 Reason to Ditch Google
summary: “Privacy is already dead. It’s impossible to stay private online.” This just isn't true. There's numerous ways to maintain privacy despite what many think.
featured-img: google-evil-logo
categories: Opinions
---

# Google Steals Cookies From Your Privacy Jar

I assume you're well aware of this.

Google has been under fire for their [privacy practices](https://www.theguardian.com/technology/2022/jan/24/google-sued-privacy-texas-district-of-columbia){:target="_blank"} in recent times.

This compelled me to purge Google from my digital life entirely (well, almost).

While I wanted to share my insights about privacy, I found I had a tough time explaining to people why it’s a big deal.

That was until I came across a [video](https://www.youtube.com/watch?v=TdbIk__Yafs){:target="_blank"} that clarified my thoughts about this.

Below are the main takeaways from the video.

Maybe this will convince you enough to ditch Google altogether...

---

> ### “If you don’t have anything to hide, you don’t need to care about privacy.”

Quite a few take this stance. You might have this opinion yourself actually.

If you do side with this, are you absolutely positive you have *nothing* to hide?

Let’s bring up some scenarios where you might want a bit of privacy...

- Texting your friends that you’re taking a *fake* sick day from work.
- Speaking negatively about your employer via group text with your family.
- Looking up certain things online that when taken out of context, would be viewed negatively by your peers.
- Sending a special interest an explicit pic of yourself.

While this might seem harmless, let’s think about it in a different context.

What if legislation gets passed that requires *ALL* companies that store *ALL* your data (i.e. messages, emails, pictures, tweets, posts, etc.) to give this information to your employer?

Now imagine if Google or Apple were required to release *ALL* of this data to your employer...

You could find yourself jobless after your boss found out you took a *fake* sick day or spoke bad about the company.

Not cool right?

Well, let’s bring up a slightly different scenario.

What if companies were required to hand over your personal data to your bank or insurance?

You might lose your health insurance if there’s a picture of you smoking a cigarette (after you told your insurance that you're a non-smoker).

I think it's safe to assume that *everyone* has the proverbial skeleton in the closet.

While the above scenarios aren’t illegal, they could land you in hot water if legislation pertaining to data protection gets changed for the worse (slightly scary thing to think about for sure).

---

> ### “Privacy is already dead. It’s impossible to stay private online.”

This just isn't true.

There are numerous ways to maintain privacy despite what many think.

Awesome web browser extensions like [uBlock Origin](https://ublockorigin.com/){:target="_blank"} and [DecentralEyes](https://decentraleyes.org/){:target="_blank"} help protect your privacy while surfing the web.

You can also use privacy-respecting search engines like [DuckDuckGo](https://duckduckgo.com/){:target="_blank"}, [StartPage](https://startpage.com){:target="_blank"}, and [Ecosia](https://ecosia.com){:target="_blank"}.

Even things like your [email service](https://tutanota.com){:target="_blank"}, [cloud storage](https://nextcloud.com){:target="_blank"}, and even your [phone’s operating system](https://lineageos.org){:target="_blank"} can be substituted with a privacy-friendly alternative.

You have a choice and say on this matter.

And it goes without saying that privacy is not dead.

For instance, [legislation](https://ec.europa.eu/info/law/law-topic/data-protection/data-protection-eu_en){:target="_blank"} gets passed in the European Union to make sure companies don’t have their way with your data.

It’s not to say privacy isn’t easy.

You may have to sacrifice both comfort and usability to learn a new program or tool for your use case.

The good news is that applications are available that are good enough and private enough to replace the ones you currently use.

---

> ### “They offer free services that simply work.”

Yeah, it might be true that free services like Google Suite are a sweet deal.

But at the same time, you’re sacrificing your personal data for free access.

Always keep in mind that free services are not *free*.

You’re paying the price with your personal data. And this data allows companies like [Google](https://proton.me/blog/google-privacy-problem){:target="_blank"} to build a comprehensive profile of you.

Everything you view, your digital work, and basically all of your exchanges online are tracked via these free services.

I am not okay with this. Personally, this isn’t a price that I’m willing to pay.

Like I mentioned before, there are [alternative services](https://www.privacytools.io/){:target="_blank"} that are just as good. Even better, they actually respect your privacy.

I think boils down to one thing...

Is the time you’re going to lose finding or relearning a new service worth it for the sake of keeping your personal data private?

While you’re entitled to your opinion, I think that yes, it is 100% worth the effort.

You privacy matters. And it’s high time to ditch Google (and big tech) entirely.
