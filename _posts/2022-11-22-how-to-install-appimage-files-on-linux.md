---
layout: post
title: How To Install AppImage Files On Linux
summary: Installing applications on Linux can be straightforward or a slight headache. I think this is especially true when it comes to installing AppImage file applications. There are certain nuances to this process. And overlooking the finer details can definitely cause frustration.
featured-img: linux-circle-hero
categories: Guides
---

# Get Up And Running With AppImage Files

Installing applications on Linux can be straightforward or a slight headache.

I think this is especially true when it comes to installing AppImage file applications.

There are certain nuances to this process. And overlooking the finer details can definitely cause frustration.

Luckily in this article, you'll learn how to install and setup AppImage files on Linux properly.

Let's jump into it now...

---

**Step 1: Download the AppImage file into your downloads folder.**

For this example, we'll be downloading as well as installing the [Standard Notes](https://standardnotes.com/download/linux) AppImage file.

<img src="./../assets/img/posts/installing-appimage-files/standard-notes-download-linux.png" style="max-height: 900px !important">

---

**Step 2: Create an Applications folder in your home directory.**

Within your Linux Operating system (whether that be Ubuntu, Manjaro, Mint, etc.), open up the Terminal application and enter in the following command:

```
mkdir ~/Applications
```

The `mkdir` command is used to create a directory (or folder).

While `~/Applications` specifies the name of the directory to be created within the `~/` (home) directory.

The `~/Applications` directory is where we'll be storing the AppImage file.

You can use this directory to store other AppImage files as well.

---

**Step 3: Move your AppImage file into the newly created Applications directory.**

First we'll list the files in the `~/Downloads` directory to make sure the AppImage file we want to move is present.

Let's open up the Terminal again. Then enter the following:

```
ls ~/Downloads
standard-notes-3.100.17-linux-x86_64.AppImage
```

Here the `ls` command lists out the contents inside of the `~/Downloads` directory.

After confirming that our Standard Notes AppImage file exists, we'll use the `mv` command to move the AppImage file from the `~/Downloads` folder into the `~/Applications` folder.

We can do this like so in the Terminal:

```
mv ~/Downloads/standard-notes-3.100.17-linux-x86_64.AppImage ~/Applications/standard-notes.AppImage
```

Notice that we're renaming the AppImage file to something shorter so that it's more readable.

---

**Step 4: Use the `chmod` command to grant yourself permission to open the AppImage file.**

Next, enter this command into the Terminal:

```
chmod +x ~/Applications/standard-notes.AppImage
```

This part is important.

If we don't perform this step, you won't be able to open the Application.

`chmod` indicates we're changing the access mode for this particular file.

While the `+x` portion allows us to make the AppImage file executable.

---

**Step 5: Open the AppImage application.**

You can now launch the Standard Notes application within the Terminal using this command:

```
~/Applications/standard-notes.AppImage
```

Here we're testing to see if the AppImage file actually launches.

After confirming that the AppImage file works, we can move onto the last step.

---

**Step 6: Create a `.desktop` file for the AppImage application.**

If you skip this step, your AppImage application won't appear when you search for it.

That's because the `.desktop` file acts as a standalone link to the AppImage application.

It helps make the AppImage file searchable basically.

Let's start by using the `touch` command to create the `standard-notes.desktop` file inside the Terminal.

```
touch ~/.local/share/applications/standard-notes.desktop
```

Here, we're creating the `.desktop` file inside `~/.local/share/applications` - this is the directory location where local `.desktop` files are stored.

Next, you will want to open up `~/.local/share/applications/standard-notes.desktop` with a text editor (i.e. Nano, Vim, VSCode, etc.)

Afterwards, copy and paste the content below into the `standard-notes.desktop` file.

```s
[Desktop Entry]
Name=Standard Notes
Comment=End-to-end encrypted notes app
Exec=/home/<your_username_goes_here>/Applications/standard-notes.AppImage
Icon=standard-notes.png
Terminal=false
Type=Application
StartupNotify=true
Categories=GNOME;GTK;Utility;
```
Lastly, edit the portion with `<your_username_goes_here>` with your **username**.

If you need help finding this information, you can enter `whoami` in the terminal.

---

## You're all done.

You've just learned how to install and setup AppImage file applications in Linux!

The great thing is that these steps can be used to install and setup other AppImage applications as well.

---

## Share your thoughts...

Feel free to leave a comment about your experiences with installing AppImage files.

Were they positive or negative?

Anything else you'd like to add? Share below...
