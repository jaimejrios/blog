---
layout: post
title: The Best Linux Alternatives To Windows and macOS
summary: The Linux distros I'll be discussing are elementaryOS and Linux Mint. I think elementaryOS is perfectly tailored for macOS users wanting to switch over to Linux. While Linux Mint is suited for Windows users.
featured-img: linux-penguin
categories: Privacy
---

# It was nice knowing you, macOS & Windows...

Switching from using Windows to macOS (or vice-versa) can be painful.

Having to relearn new keyboard commands, how to access certain settings, and making your desktop a comfortable, digital workspace takes time.

While you probably don't want to take the time to ditch Windows and macOS altogether, [this blog post](https://blog.jaimejrios.com/the-1-reason-to-ditch-google/) might have you think otherwise.

I chose to leave Windows and macOS because one, I don't agree with their [privacy](https://www.cnet.com/tech/services-and-software/duckduckgo-will-block-more-microsoft-tracking-scripts/) [practices](https://www.politico.eu/article/apple-privacy-problem/).

And two, I don't want to be glued to a single company's ecosystem of applications.

I wanted out of both Google and Apple. And big tech in general.

But in order to do that, I had to figure out which version of Linux to install.

---

## Come again? What is Linux? What's a distro?

A **distro** is a colloquial term used in the Linux universe.

It's short for **distribution** or a specific version of Linux.

And **Linux** itself is simply the **kernel**, or the software that helps the *hardware* (the physical components of a computer) and the *operating system* (how we as users interface with the computer) communicate with each other.

Both Windows and macOS are operating systems that help us interface with the hardware components of our computer so we can tell it what to do.

And to recap, a Linux **distro** (or a Linux distribution) is a version of a Linux operating system.

[Ubuntu](https://ubuntu.com/) is a great example of a Linux distro.

Now that we have the basic Linux terminology out of the way, let's look at two Linux distributions.

These distros in my opinion, can replace either your Windows or macOS operating systems.

---

## But first, let's talk about Ubuntu real quick.

The Linux distros I'll be discussing are [elementaryOS](https://elementary.io/) and [Linux Mint](https://linuxmint.com/).

I think elementaryOS is perfectly tailored for macOS users wanting to switch over to Linux. While Linux Mint is suited for Windows users.

elementaryOS and Linux Mint are both modified versions of [Ubuntu](https://ubuntu.com/).

<img src="./../assets/img/posts/best-linux-alternatives/ubuntu-desktop.jpg">
*The Ubuntu Operating System*

Ubuntu was created and is currently owned by a company called [Canoncial](https://canonical.com/) (in case you're curious).

Ubuntu is probably the most well-known Linux distro on the planet.

According to [W3Tech]([https://w3techs.com/technologies/details/os-linux), Ubuntu has a market share of 33.9% within the Linux subcategory space.

You might be asking, why is elementaryOS and Linux Mint based off Ubuntu?

Well there's a few reasons for this...

1. The community for Ubuntu is massive.

2. Finding help and troubleshooting Ubuntu is straightforward.

3. Ubuntu is very stable and reliable.

---

## Why choose Linux Mint?

[Linux Mint](https://linuxmint.com/) resembles Windows in both visual aesthetics and functionality.

<img src="./../assets/img/posts/best-linux-alternatives/linux-mint-desktop.png">

Simply put, you won't get caught off guard when starting Linux Mint for the first time.

The menu bars, application icons, and tools are laid out in similar fashion to Windows.

This feels great and wonderfully familiar.

Finding applications and files on Linux Mint is a breeze.

To put icing on the cake, Linux Mint can also run on lower-end machines.

This is awesome if you're looking to squeeze some juice out of an older computer.

---

## Why elementaryOS?

If you love macOS, I think you'll fall for [elementaryOS](https://elementary.io/).

<img src="./../assets/img/posts/best-linux-alternatives/eos-desktop-starbook.jpg">

The way the operating system functions feels as though you're using an Apple machine.

elementaryOS places lots of emphasis on minimalistic design and functionality like its macOS counterpart.

Having used macOS for 5+ years, I appreciate how easy it is to find applications and files on this distro.

<img src="./../assets/img/posts/best-linux-alternatives/eos-desktop-finder.png">

The only downside I've seen with elementaryOS is that you need higher specs to run it.

A dual-core computer processor probably won't cut it for running elementaryOS (unlike Linux Mint).

---

## What Linux Mint and elementaryOS do well...

1. The installation process is straightforward - concise steps are laid out to help you install elementaryOS or Linux Mint on your computer from a USB drive.
2. Both distros provide helpful, first-time login notifications to show you where's where on your desktop.
3. Settings are easy to find and change.
4. Pre-installed office applications and tools are there to help you jump right into productivity mode as soon as you login.
5. They have rock-solid communities - there's lots of help via forums (like on [askubuntu](https://askubuntu.com/)).

---

## Any Caveats with elementaryOS and Linux Mint?

Software compatibility can be an issue if you want to run specific programs like Microsoft Office.

While you can use a web browser for most things like Google Drive and Microsoft Office 365, sometimes you need an application that runs on a particular operating system.

While you might not find suitable alternatives for specific macOS and Windows applications, this begs the question of...

Is sacrificing the perks of Windows and macOS worth it?

Do the benefits outweigh the costs so you can finally take back your privacy and [break free from big tech](https://blog.jaimejrios.com/how-to-break-free-from-big-tech/)?

If you're concerned about the current state of digital privacy like I am, then yes, I think it is 100% worth of effort to opt for privacy-centric alternatives like [elementaryOS](https://elementaryos.io/) and [Linux Mint](https://linuxmint.com/).

---

## Share your thoughts...

Feel free to leave a comment about your experiences using Linux distros.

Were they positive or negative?

Anything else you'd like to add? Share below...
