---
layout: page
title: About
permalink: /about/
---

Apart from being a techy, I'm also a fitness enthusiast. I exercise year round and take part in yoga sessions from time to time. I also don't mind a nice round of frisbee golf with friends.

When forced indoors, there's nothing better than a gaming session along with binge watching my favorite sci-fi, anime, and YouTube programs. There'll be moments where my sweet tooth takes over, and that's when I'll need a healthy dose of vegan goodies. Apart from that, I like exploring the rabbit hole of the latest tech trends in the IT world.
