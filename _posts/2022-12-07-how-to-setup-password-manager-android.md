---
layout: post
title: How To Setup A Password Manager On Your Android Phone
summary: Bitwarden allows you to store and use passwords without having to remember them. You just use a single Bitwarden password to access the rest of your passwords for each of your online accounts.
featured-img: bitwarden-android-hero
categories: Guides
---

# No more memorizing Passwords.

```
GeorgeOrwell1984
```

I've seen this password pattern used too many times. A first name, a last name, followed by a year (or something along those lines).

Passwords seem easier to remember this way, right?

I actually used to make my passwords like this. I still ended up forgetting them (just goes to show how reliable memory can be).

Also making passwords like this is not safe. Someone who knows you personally, or a talented hacker can crack your password with relative ease.

Luckily, an awesome application called Bitwarden came to my rescue. I learned about it using [alternativeto.net](https://alternativeto.net).

Bitwarden allows you to store and use passwords without having to remember them.

You just use a single Bitwarden password to access the rest of your passwords for each of your online accounts.

Basically, no more forgetting and resetting passwords.

In this post, I've outlined steps for how to install and setup the Bitwarden password manager on your Android smartphone.

Let's get started...

---

**Step 1 - Write down all of your passwords on a notepad.**

This is probably the most important step.

Here, it's imperative that you write down every single username and password for each of your registered online accounts.

This might seem tedious (it is), but it will make things much easier since we'll be entering them into the Bitwarden application in a later step.

---

**Step 2 - Find and Install Bitwarden via the App Store.**

Now that you've written down all of your passwords, you'll want to navigate to the Google Play Store on your Android phone.

<img src="./../assets/img/posts/bitwarden-android-setup/2-play-store.png" style="max-height: 600px !important">

From there, tap on the search bar at the top of the screen.

<img src="./../assets/img/posts/bitwarden-android-setup/2-play-store-search-bar.png" style="max-height: 600px !important">

Next, type "Bitwarden" in the search. Then tap on the "Bitwarden" search result.

<img src="./../assets/img/posts/bitwarden-android-setup/3-play-store-search.png" style="max-height: 600px !important">

Press the *Install* button next to the Bitwarden logo.

<img src="./../assets/img/posts/bitwarden-android-setup/4-play-store-bitwarden-install.png" style="max-height: 600px !important">

After Bitwarden finishes installing, tap *Open*.

<img src="./../assets/img/posts/bitwarden-android-setup/5-play-store-bitwarden-open.png" style="max-height: 600px !important">

Now that you have Bitwarden installed, let's create your free Bitwarden account.

---

**Step 3 - Create a free Bitwarden account.**

After installing Bitwarden, open the application. Then tap on the *Create Account* button.

<img src="./../assets/img/posts/bitwarden-android-setup/6-bitwarden-create-account.png" style="max-height: 600px !important">

It's time to create a *master password* for Bitwarden.

This will act as your main gatekeeper password for the rest of your passwords.

So make it strong, but also make it memorable. It will be the only password you'll need to remember (bear that in mind).

You'll also need to fill in the Email Address and Master Password fields to complete the Create Account process.

Afterwards, tap on the Submit button.

<img src="./../assets/img/posts/bitwarden-android-setup/7-bitwarden-create-password.png" style="max-height: 600px !important">

After creating your Bitwarden account, you'll then be sent to your Bitwarden vault.

<img src="./../assets/img/posts/bitwarden-android-setup/8-bitwarden-my-vault.png" style="max-height: 600px !important">

Here you'll be able to create unique, secure passwords for each of your online accounts.

We're almost done! Only two steps left.

---

**Step 4 - Saving your passwords into the Bitwarden Vault.**

Alright, let's break out your notepad with all of your written-down passwords. This will come in handy for this step...

Now that you have your notepad handy, inside the Bitwarden app, tap the *Add An Item* button.

<img src="./../assets/img/posts/bitwarden-android-setup/8-bitwarden-add-item.png" style="max-height: 600px !important">

Here's how you add a password to store in your Bitwarden Vault.

First, you'll want to enter in the name of your online account as well as the username. In this example we're using *reddit.com*.

Afterwards, you will need to generate a password for your online account.

To do this, press on the circle icon under the *password* field.

<img src="./../assets/img/posts/bitwarden-android-setup/9-bitwarden-add-to-vault.png" style="max-height: 600px !important">

Bitwarden will then generate a password for you. If you're satisfied with the password, press the *Select* option at the top right corner.

If you want to generate another password, press the circle icon under *Select*.

<img src="./../assets/img/posts/bitwarden-android-setup/10-bitwarden-generate-password.png" style="max-height: 600px !important">

After pressing *Select*, you will be redirected to the previous *Edit Item* screen. From there, add the URl for your item.

For this example, we'll be using the *https://reddit.com* URl.

If we were using eBay instead of Reddit for instance, the URl would be *https://ebay.com/*

After entering your URl, hit *Save*.

<img src="./../assets/img/posts/bitwarden-android-setup/11-bitwarden-add-url.png" style="max-height: 600px !important">

Congrats! You've saved your first Bitwarden password to  your vault.

To add the rest of your passwords to the Bitwarden application, follow the instructions outlined in Step 4.

We're onto the last step!

All that's left is enabling Bitwarden's autofill password feature so you won't have to memorize your password when logging into an online account.

---

**Step 5 - Autofill Passwords with Bitwarden**

We're almost done!

By the end of this step you will be able to autofill passwords whenever you try logging into your online accounts.

The first thing we'll need to do is change the Vault Timeout settings within the Bitwarden app.

Tap on the gear icon at the bottom right corner to navigate to Bitwarden's app settings.

Then, under the *Vault Timeout* option, set the timeout value to *Never*.

<img src="./../assets/img/posts/bitwarden-android-setup/12-bitwarden-vault-timeout.png" style="max-height: 600px !important">

After making that change, we need to enable Bitwarden's autofill service.

To start, you'll want to tap on the Auto-fill services option.

<img src="./../assets/img/posts/bitwarden-android-setup/13-bitwarden-autofill-services.png" style="max-height: 600px !important">

Next, tap on the Auto-fill service option to turn it on.

<img src="./../assets/img/posts/bitwarden-android-setup/14-bitwarden-autofill-service.png" style="max-height: 600px !important">

Under the Autofill service menu, select the Bitwarden option.

<img src="./../assets/img/posts/bitwarden-android-setup/15-bitwarden-select-service.png" style="max-height: 600px !important">

Now for the moment of truth...

Let's open up the login screen to Reddit to see how Bitwarden's autofill password feature works.

Your days of struggling to remember your online passwords will soon be over!

<img src="./../assets/img/posts/bitwarden-android-setup/16-reddit-autofill-option.png" style="max-height: 600px !important">

And voila!

You'll notice that when you try entering in your email or password, Bitwarden's autofill service will appear  to let you know that you have a saved password for Reddit.

All you need to do is tap on the Bitwarden field to autofill the email and password fields.

<img src="./../assets/img/posts/bitwarden-android-setup/17-reddit-autofill.png" style="max-height: 600px !important">

And that is it! Now you can login with ease.

You've just learned how to use Bitwarden to save yourself the headache of remembering passwords.

---

## Share your thoughts...

Feel free to leave a comment about your experiences using password managers. Were they positive or negative?

Anything else you'd like to add? Share below...
