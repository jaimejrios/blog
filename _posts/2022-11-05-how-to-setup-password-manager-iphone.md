---
layout: post
title: How To Setup A Password Manager On Your iPhone
summary: Bitwarden allows you to store and use passwords without having to remember them. You just use a single Bitwarden password to access the rest of your passwords for each of your online accounts.
featured-img: bitwarden-apple-hero
categories: Guides
---

# Time To Stop Memorizing Passwords

```
TinaLewis1988
```

I've seen this password pattern used too many times. A first name, a last name, ending with a date of birth (or something along those lines).

Passwords seem easier to remember this way, right?

I actually used to make my passwords like this.

But I still ended up forgetting them (just goes to show how reliable memory can be).

Also making passwords like this is not safe.

Someone who knows you personally, or a talented hacker can crack your password with relative ease.

Luckily, an awesome application called Bitwarden came to my rescue. I learned about it using [alternativeto.net](https://alternativeto.net).

Bitwarden allows you to store and use passwords without having to remember them.

You just use a single Bitwarden password to access the rest of your passwords for each of your online accounts.

Basically, no more forgetting and resetting passwords.

In this post, I've outlined steps for how to install and setup the Bitwarden password manager on your iPhone.

Let's get started...

---

**Step 1 - Write down all of your passwords on a notepad.**

This is probably the most important step.

Here, it's imperative that you write down every  username and password for each of your registered online accounts.

This might seem tedious (it is), but it will make things much easier for when you be enter them into the Bitwarden application.

This post will explain in detail how to add a single password into Bitwarden.

From there you can add the rest of your passwords from your notepad onto the Bitwarden app.

---

**Step 2 - Find and Install Bitwarden via the App Store.**

Now that you've written down all of your passwords, you'll want to navigate to the App Store on your iPhone.

<img src="./../assets/img/posts/bitwarden-apple-setup/2-2-app-store-home.PNG" style="max-height: 600px !important">

From there, tap on the search icon at the bottom right.

<img src="./../assets/img/posts/bitwarden-apple-setup/2-3-app-store-search.PNG" style="max-height: 600px !important">

Next, type "Bitwarden" in the search. Then tap on the "Bitwarden" search result.

<img src="./../assets/img/posts/bitwarden-apple-setup/2-4-app-store-search-results.PNG" style="max-height: 600px !important">

Press the **Get** button next to the Bitwarden logo.

<img src="./../assets/img/posts/bitwarden-apple-setup/2-5-app-store-get-bitwarden.PNG" style="max-height: 600px !important">

After Bitwarden finishes installing, tap **Open**.

<img src="./../assets/img/posts/bitwarden-apple-setup/2-6-app-store-open-bitwarden.PNG" style="max-height: 600px !important">

Now that you have Bitwarden installed, let's create your free Bitwarden account.

---

**Step 3 - Creating a free Bitwarden account.**

After installing Bitwarden, open the application. Then tap on the **Create Account** button.

<img src="./../assets/img/posts/bitwarden-apple-setup/3-1-create-account.PNG" style="max-height: 600px !important">

It's time to create a *master password* for Bitwarden.

This will act as your main gatekeeper password for the rest of your passwords.

So make it strong, but also make it memorable. It will be the only password you'll need to remember (bear that in mind).

You'll need to fill in the **Email Address** and **Master Password** fields to complete the **Create Account** process.

After completing the **Create Account** form, tap on the **Submit** button.

<img src="./../assets/img/posts/bitwarden-apple-setup/3-2-create-password.PNG" style="max-height: 600px !important">

After creating your Bitwarden account, you'll then be sent to your Bitwarden vault.

<img src="./../assets/img/posts/bitwarden-apple-setup/3-3-the-vault.PNG" style="max-height: 600px !important">

Here you'll be able to create unique, secure passwords for each of your online accounts.

We're almost done! Only two steps left.

---

**Step 4 - Saving your passwords into the Bitwarden Vault.**

Alright, let's break out your notepad with all of your written-down passwords. This will come in handy for this step...

Now that you have your notepad handy, inside the Bitwarden app, tap the **Add An Item** button.

<img src="./../assets/img/posts/bitwarden-apple-setup/4-0-add-an-item.PNG" style="max-height: 600px !important">

Here's how you add a password to store in your Bitwarden Vault...

First, you'll want to enter in the **Name** of your online account as well as the **username**.

In this example we're using *mail.google.com* for the **Name** field and *johndoe2022@gmail.com* for the **Username**.

Afterwards, you will need to generate a password for this account.

To do this, press on the circle icon under the **password** field.

<img src="./../assets/img/posts/bitwarden-apple-setup/4-1-add-to-vault.PNG" style="max-height: 600px !important">

Bitwarden will then generate a password for you.

If you're satisfied with the password, press the **Select** option at the top right corner.

If you want to generate another password, press the circle icon under the **Select** option.

<img src="./../assets/img/posts/bitwarden-apple-setup/4-2-generate-password.PNG" style="max-height: 600px !important">

After pressing **Select**, you will be redirected to the previous **Edit Item** screen.

From there, add the **URl** (uniform resource locator) for your item.

For this example, we'll be using the *https://google.com* URl (Gmail uses the *google.com* URl for all its services).

If we were using eBay instead of Gmail for instance, the URl would be *https://ebay.com/*

After entering your URl, hit **Save**.

<img src="./../assets/img/posts/bitwarden-apple-setup/4-3-add-url.PNG" style="max-height: 600px !important">

Congrats! You've saved your first Bitwarden password to  your vault.

To add the rest of your passwords to the Bitwarden application, follow the instructions outlined in **Step 4**.

We're onto the last step!

All that's left is enabling Bitwarden's autofill password feature so you won't have to memorize your password when logging into an online account.

---

**Step 5 - Autofill Passwords with Bitwarden**

We're almost done!

By the end of this step you will be able to autofill passwords whenever you try logging into an online account.

The first thing we'll need to do is change the **Vault Timeout** settings within the Bitwarden app.

Tap on the **Gear** icon at the bottom right corner to navigate to Bitwarden's app settings.

Then, under the **Vault Timeout** option, set the timeout value to **Never**.

<img src="./../assets/img/posts/bitwarden-apple-setup/5-1-vault-timeout-settings.PNG" style="max-height: 600px !important">

After making that change, follow the instructions outlined below to enable Bitwarden's autofill settings.

<img src="./../assets/img/posts/bitwarden-apple-setup/5-2-password-autofill.PNG" style="max-height: 600px !important">

Now for the moment of truth...

Let's open up the login screen to Gmail to see how Bitwarden's autofill password feature works.

Your days of struggling to remember your online passwords will soon be over!

<img src="./../assets/img/posts/bitwarden-apple-setup/5-3-account-sign-in.PNG" style="max-height: 600px !important">

And voila!

You'll notice that when you try entering in your email and/or password, Bitwarden will appear above the keyboard to let you know that you have a saved password for Gmail.

All you need to do is tap on the Bitwarden field above the keyboard to autofill the email and password fields.

<img src="./../assets/img/posts/bitwarden-apple-setup/5-4-password-autofill.PNG" style="max-height: 600px !important">

And that is it! Now you can login with ease.

You've just learned how to use Bitwarden to save yourself the headache of remembering passwords.

---

## Share your thoughts...

Feel free to leave a comment about your experiences using password managers.

Were they positive or negative?

Anything else you'd like to add? Share below...
