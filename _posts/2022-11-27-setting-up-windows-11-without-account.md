---
layout: post
title: 3 Steps To Setup Windows 11 Without A Microsoft Account
summary: Windows 11 now forces you to login with a Microsoft account during initial setup (unlike Windows 10). For some (including myself), the last thing you want to do is create a Microsoft Account.
featured-img: windows-11-hero
categories: Guides
---

# Microsoft Thought They Could Get Away With One

Windows 11 now forces you to login with a Microsoft account during initial setup (unlike Windows 10).

For some (including myself), the last thing you want to do is create a Microsoft Account.

Especially since [Microsoft has come under fire](https://www.forbes.com/sites/daveywinder/2019/08/28/microsoft-confirms-windows-10-privacy-investigation-with-a-4-billion-sting/?sh=78c133947b1e) in recent times because of the way they're collecting user data.

Not all hope is lost though. You can in fact setup Windows 11 without a Microsoft Account using a clever workaround.

This method works as of **11/27/2022** (this could change though).

Now let's get started...

---

### Step 1: Disconnect Your Internet Connection

When Microsoft prompts you to **Select Your Region** on initial setup, you will want to disconnect from the internet.

![selecting your region - windows 11 setup](./../assets/img/posts/windows-11-setup-no-account/0-select-region.jpg)

You can do the following to disconnect from the internet...

- Press `Shift + F10` on your keyboard. This will open the **Command Prompt**.
- Inside the Command Prompt enter:

```
ipconfig /release
```
![typing ipconfig \release inside the command prompt](./../assets/img/posts/windows-11-setup-no-account/1-ipconfig-release.jpg)

Now don't close the Command Prompt just yet.

We still need to perform another step...

---

### Step 2: Enable Out-Of-Box Experience and Disable Internet Requirement

With the Command Prompt still open, enter the following:

```
oobe\bypassnro
```
![typing oobe/bypassnro inside the command prompt](./../assets/img/posts/windows-11-setup-no-account/2-oobe-bypass.jpg)

Please note that this command is a single phrase without any spaces.

This command will disable the internet requirement during initial setup. You will then be able to setup Windows 11 without a Microsoft account.

After entering this command, your computer will reboot automatically.

When the Windows 11 setup resumes, you will start back up at the **Select Your Region** screen.

---

### Step 3: Proceed With The Windows 11 Setup Process.

Now for the easy part...

All that's left is to go through each setup page to finish setting up Windows 11.

- First, you'll want to **Select Your Region**.
- Then click the **Yes** button.

![Windows 11 Setup - Select Your Region](./../assets/img/posts/windows-11-setup-no-account/0-select-region.jpg)

---

- Next, you'll want to **Select Your Keyboard Layout**.
- After selecting it, click the **Yes** button.

![Windows 11 Initial Setup - Select Keyboard](./../assets/img/posts/windows-11-setup-no-account/2-select-keyboard.jpg)

- Afterwards, click the **Skip** button when adding a second layout.

![Windows 11 Initial Setup - Skip Second Keyboard Layout](./../assets/img/posts/windows-11-setup-no-account/2-skip-2nd-keyboard.jpg)

---

- You'll then be prompted to **Review the Windows License Agreement**.
- After reviewing it, click the **Accept** button.

![Windows 11 Initial Setup - Accept License Agreement](./../assets/img/posts/windows-11-setup-no-account/3-accept-terms.png)

---

- Next up is **Entering Your Name** for your computer (this will be the name used to sign in).
- Then click the **Next** button.

![Windows 11 Initial Setup - Enter Name For Device](./../assets/img/posts/windows-11-setup-no-account/4-enter-name.jpg)


---

- After entering your name, you'll then be prompted to **Create A Password** for the account.
- Then click the **Next** Button

![Windows 11 Initial Setup - Create Your Password](./../assets/img/posts/windows-11-setup-no-account/5-create-password.jpg)

- If you created a password, you will be required to complete **three security questions** to recover the account in case you forget your password.

---

- All that's left is **Choosing Your Privacy** settings.
- After choosing your privacy settings, click the **Accept** button.

![Windows 11 Initial Setup - Create Your Password](./../assets/img/posts/windows-11-setup-no-account/6-choose-privacy-settings.jpg)

---

Windows 11 will then get things ready for you.

It will take just a few minutes before you have Windows 11 up and running.

And that is it!

You've just setup Windows 11 without needing to create a Microsoft account.

---

## Share your thoughts...

Was this tutorial helpful?

Feel free to leave a comment about your experience setting up Windows 11.

Was it positive or negative?

Anything else you'd like to add? Share below...